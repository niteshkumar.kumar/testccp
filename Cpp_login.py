import allure
from selenium import webdriver

from allure_commons.types import AttachmentType

import unittest

import HtmlTestRunner

#from selenium.webdriver.common.desired_capabilities import  DesiredCapabilities


#current_directory = os.getcwd()

class cpptest(unittest.TestCase):

    @classmethod
    def setUp(self) -> None:
        self.driver = webdriver.Chrome(executable_path='C://Program Files (x86)/Jenkins/plugins/chromedriver/WEB-INF/lib/chromedriver.exe')

        self.driver.maximize_window()

        self.driver.implicitly_wait(2)

    def testcpptest(self):
        driver = self.driver



        self.driver.get('http://cpp-sprint.s3-website-us-east-1.amazonaws.com/')

        self.driver.find_element_by_xpath("/html/body/div/div[2]/div[1]/div/form/div[1]/input").send_keys('9599665541')

        self.driver.find_element_by_xpath('//*[@id="l_box"]/form/div[2]/input').click()

        self.driver.find_element_by_xpath('//*[@id="l_box"]/div[3]/input[1]').send_keys('1')

        self.driver.find_element_by_xpath('//*[@id="l_box"]/div[3]/input[2]').send_keys('2')

        self.driver.find_element_by_xpath('//*[@id="l_box"]/div[3]/input[3]').send_keys('3')

        self.driver.find_element_by_xpath('//*[@id="l_box"]/div[3]/input[4]').send_keys('4')

        self.driver.find_element_by_xpath('//*[@id="l_box"]/div[4]/button').click()

        self.driver.find_element_by_xpath('/html/body/div/div[4]/div/div[2]/div[2]/div/div/div[1]/input').click()

        self.driver.find_element_by_xpath('/html/body/div/div[8]/div/div/div[3]/div/div/button').click()

        self.driver.find_element_by_xpath('/html/body/div/div[4]/div/div[2]/div[2]/div/div/div[4]/a/input').click()

        self.driver.find_element_by_xpath('/html/body/div/div[3]/div/div[1]/form/div/div/div/div/div/div[5]/div/div[2]/input').click()

        self.driver.find_element_by_name('card_number1').send_keys('1')

        self.driver.find_element_by_name('card_number2').send_keys('5')

        self.driver.find_element_by_name('card_number3').send_keys('4')

        self.driver.find_element_by_name('card_number4').send_keys('6')

        self.driver.find_element_by_name('card_number5').send_keys('2')

        self.driver.find_element_by_name('card_number6').send_keys('7')

        self.driver.find_element_by_name('card_issuer').click()

        self.driver.find_element_by_partial_link_text('Lorem Ipsum').click()

        self.driver.find_element_by_partial_link_text('card_type').click()

        self.driver.find_element_by_partial_link_text('Master Card').click()

        self.driver.find_element_by_partial_link_text('card_nick_name').click()

        self.driver.find_element_by_partial_link_text('Cardio').click()

        self.driver.find_element_by_xpath('//*[@id="root"]/form/div/div/div[1]/div[2]/div[3]/div[2]/div[2]/div/div/input').click()




    def tearDown(self) -> None:
        self.driver.close()

        self.driver.quit()

        print('Test Passed')


if __name__ == "__main__":
    unittest.main(testRunner=HtmlTestRunner.HTMLTestRunner(output='..CPP_Project/Reports'))




#t
